(function(){
	// grab the DOM elements we want to manipulate
	var viewport = $('#viewport');
	var level = $('#level');
	var background = $('#background');
	var stats = $('#stats');
	var playbutton = $('#playbutton');

	var spriteCount = 0;
	var levelSprites = [];
	var levelSpriteCount = 0;
	var monsters = [];

	// timer and stats
	var currentTimestamp = +new Date();
	var previousTimestamp = 0;
	var framesThisSecond = 0;
	var elapsedMs = 0;
	var currentFPS = 60;

	// adding Tardis
	$('<div/>',{
	  'class': 'sprite tardis',
	  'style': "bottom:0;"
	}).appendTo( viewport );

	// adding the Doctor
	$('<div/>',{
	  'class': 'sprite doctor',
	  'style': 'right:0;top:0',
	  'id': 'doctor'
	}).appendTo( viewport );

	var addMonster = function() {
		console.log( 'addin monster' );
	  $('<div/>',{
        'class': 'sprite dalek1',
		'id': 'monster_' + monsters.length,
		'style': 'left: ' + Math.round((viewport.width()/2)-20) + 'px;bottom:' + Math.round((viewport.height()/2)-40) + 'px;'
	  }).appendTo( viewport );

	  monsters.push( 'monster_' + monsters.length );

	  // TODO this should be controller somewhere else ...
	  window.setTimeout( addMonster, 10000 );
	}

	var startGame = function() {
		animate();
	}

	// TODO refactor, move position calculate and move into separate function
	var moveDoctor = function( direction ) {
		var doctor = $('#doctor'),
			doctor_right = parseInt(doctor.css('right'),10),
			doctor_top = parseInt(doctor.css('top'),10),
			jump = 10;

		  switch( direction ) {
		    case 'up':
					doctor_top -= jump;
				break;
			case 'down':
					doctor_top += jump;
				break;
			case 'left':
					doctor_right += jump;
				break;
			case 'right':
					doctor_right -= jump;
				break;
		  }

		  doctor.css({'right': doctor_right + 'px' });
		  doctor.css({'top': doctor_top + 'px' });

		  console.log( doctor_top, doctor_right );
	}

	var animate = function() {
	  $.each(monsters, function(i,monster){
        var monster_obj = $('#'+monster),
		  pos_left = parseInt(monster_obj.css('left'),10),
		  pos_bottom = parseInt(monster_obj.css('bottom'),10),
		  jump = 10,
		  directions = ['up','down','right','left'],
          direction = directions[ Math.floor((Math.random()*directions.length)) ];

		  switch( direction ) {
		    case 'up':
					pos_bottom += jump;
				break;
			case 'down':
					pos_bottom -= jump;
				break;
			case 'left':
					pos_left -= jump;
				break;
			case 'right':
					pos_left += jump;
				break;
		  }

		  monster_obj.css({'left': pos_left + 'px' });
		  monster_obj.css({'bottom': pos_bottom + 'px' });
		  $(stats).html( 'x:' + pos_left + ' y:' + pos_bottom );
      });

	  window.setTimeout( animate, 50 );
	}

	playbutton.on( 'click', function() {
	  playbutton.hide();
	  addMonster();
	  startGame();
	});

	$(window).keydown(function(e) {
		var key = e.which;
		//do stuff with "key" here...
		switch( key ) {
			case 38: //up
				moveDoctor( 'up' );
				break;
			case 37: // left
				moveDoctor( 'left' );
				break;
			case 40: // down
				moveDoctor( 'down' );
				break;
			case 39: // right
				moveDoctor( 'right' );
				break;
		}
	});

	//viewport.append( '<div class="sprite dalek1"></div>' );
	//viewport.append( '<div class="sprite doctor"></div>' );
})();
